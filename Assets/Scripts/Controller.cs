﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour {


	public float selectDistance; //The minimum distance that the player can pickup an object
	public GameObject phone; //A reference to the iPhone game object
	public float phoneDistance; //The distance the phone is away from the player
	public float scrollSensitivity; //The mouse scroll sensitivity that controls the rotation of the phone
	public float doorOpenSensitivity;
	public float maxVelocityChange = 10.0f;
	public float speed = 10.0F; //The speed of the player
	public Transform items; //A reference to the group of items that the player picks up
	public float horizontalPhoneDistance = 0.3f;
	public float verticalPhoneDistance = 0.3f;
    private bool blind = false;
	private Camera view; //A reference to the camera of the player view
	private bool holdingItem; //Whether the player is currently holding an item
	private GameObject heldItem; //The item that is currently held by the player
	private Rigidbody rb; //A reference to the rigidbody of the player
	private Vector3 screenCenter; //The center of the screen
	private float phoneRotation; //The rotation of the phone
	private Vector3 initMousePosition;
	private bool openingDoor;
	private Transform currentDoor;
	private float phoneLeftSide;
	private float currentDoorRotation;
	private camMouseLook camera;
	private Vector3 focus;
    private Item pastItem=null;
    private Sprite centerSprite;
    private Color colorOfCenter;
    private bool inDocument;//bool to know if the player is reading a document;

    // Use this for initialization
    void Start () {
		phoneLeftSide = 1;
		Cursor.lockState = CursorLockMode.Locked;
		view = GetComponentInChildren<Camera>();
		screenCenter =  new Vector3 (.5f, .5f, view.nearClipPlane);
		holdingItem = false;
		rb = this.GetComponent<Rigidbody> ();
		phoneRotation = 135;
		openingDoor = false;
		camera = GetComponentInChildren<camMouseLook> ();
		Camera phoneCam = phone.GetComponentsInChildren<Camera> ()[0];
		phoneCam.depth = view.depth - 2;
        inDocument = false;

    }

	// Update is called once per frame
	void Update () {
        if (!holdingItem)
        {
            //Detects if there is an item
            Vector3 rayOrigin = view.ViewportToWorldPoint(screenCenter);
            RaycastHit hit;
            Ray ray = new Ray(rayOrigin, view.transform.forward);
            bool hasTarget = Physics.Raycast(ray, out hit, selectDistance, ~(1 << 9));

            //Sets up variables in case of item interaction/pickup
            Item item = null;
            LightSwitch lightSwitch = null;
            Image documentImage = null;

            if (hit.transform != null)
            {
                item = hit.transform.gameObject.GetComponent<Item>();
                lightSwitch = hit.transform.gameObject.GetComponent<LightSwitch>();
                documentImage = hit.transform.gameObject.GetComponentInChildren<Image>();
            }

            //If the player left-click
            if (Input.GetButtonDown("Fire1")){

                //Activate the animation of the item
                if (item != null && item.mode == Item.Mode.Activated && item.getAnimator() != null)
                {
                    item.getAnimator().SetTrigger("activate");
                    if (item == Mission.nextActivation.item)
                        Mission.nextActivation.activate();
                }

                //Open up the image of the document
                if (documentImage != null)
                {
                    changeImage(documentImage.sprite);
                }

                //Turn the light switch on/off
                if (lightSwitch != null)
                {
                    lightSwitch.switchLight();
                    blind = !lightSwitch.getStatus();
                }

                //Picks up the item
                if (hasTarget && item != null && item.isMoveable())
                {
                    heldItem = hit.transform.gameObject;
                    hit.transform.GetComponent<Item>().setSelectState(true);
                    holdingItem = true;
                    heldItem.transform.parent = transform;
                    heldItem.transform.localPosition = -transform.forward * selectDistance / 2;

                    //Remove the selection halo from the item
                    Shader shader = Shader.Find("Standard");
                    item.GetComponent<Renderer>().material.shader = shader;
                }
            }

            //Turns on/off the selection halo
            if (item != null)
            {
                //Turn on selection halo if looking at a moveable object
                if (hasTarget && item.isMoveable() && item.gameObject.GetComponent<Mirror>() == null)
                {
                    Shader shader = Shader.Find("Outlined/Uniform");
                    item.GetComponent<Renderer>().material.shader = shader;
                    pastItem = item;
                }
                
            }
            //Turn off selection halo from moveable object that you were looking at last frame
            else if (pastItem != null)
            {
                Shader shader = Shader.Find("Standard");
                pastItem.GetComponent<Renderer>().material.shader = shader;
                pastItem = null;
            }
        } else{
            //Drop an item
            if (Input.GetButtonDown("Fire1"))
            {
                //Drops the item
                heldItem.GetComponent<Item>().setSelectState(false);
                holdingItem = false;
                heldItem.transform.parent = null;
                heldItem = null;
            }
        }

        //Exit the reading of the Document for now the trigger key is F but after I ll change in the future
        if(inDocument && Input.GetButtonDown("Interaction"))
        {
            Image temp = this.GetComponentInChildren<Image>();
            temp.sprite = centerSprite;
            inDocument = false;
            temp.color = colorOfCenter;
            this.GetComponentInChildren<CanvasScaler>().scaleFactor = 1;
            
        }

        //This code is for selecting an item for the phone camera to constantly follow
        if (Input.GetButtonDown ("Fire2")) {
			Vector3 rayOrigin = view.ViewportToWorldPoint(screenCenter);
			RaycastHit hit;
			Ray ray = new Ray (rayOrigin, view.transform.forward);
			Physics.Raycast(ray, out hit, selectDistance, ~(1<<9));
			focus = hit.point;
		}

		//Updates the position of the currently held item
        if (heldItem != null)
		{
			Vector3 newPosition = view.ViewportToWorldPoint(screenCenter)+view.transform.forward*selectDistance/3;
            heldItem.transform.position = newPosition;
		}

		//Activate/Deactivate phone
		if (Input.GetKeyDown(KeyCode.E) && !phone.activeInHierarchy)
		{
			phone.SetActive(true);
		}
		else if (Input.GetKeyDown(KeyCode.E) && phone.activeInHierarchy) 
		{
			phone.SetActive(false);
		}

		//Update the position and rotation of the phone if it is currently active
		if (phone.activeInHierarchy)
		{
			//Switches the hand that holds the phone (left or right)
			if (Input.GetKeyDown(KeyCode.Q))
			{
				phoneLeftSide = -phoneLeftSide;
			}

			//Calculates the position of the phone for every frame

			//Calculates how much to shift the phone relative to the player
			Vector3 horizontalShift = view.transform.right * horizontalPhoneDistance * phoneLeftSide;

			//Set the position of the phone
			Vector3 newPosition = view.ViewportToWorldPoint(screenCenter) + view.transform.forward * verticalPhoneDistance - horizontalShift;
			phone.transform.position = newPosition;
			//Debug.Log (phone.transform.localRotation.eulerAngles.y);
			if (!Input.GetButton ("Fire3")) {
				
				//Allows the user to manually rotate the phone by scrolling
				phoneRotation += Input.GetAxis ("Mouse ScrollWheel") * scrollSensitivity;

				//Rotates the phone in a way that makes it face towards the player
				Quaternion rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y + phoneRotation*phoneLeftSide, transform.eulerAngles.z);
				phone.transform.rotation = Quaternion.Slerp (phone.transform.rotation, rotation, Time.deltaTime * 10);

			} else if (focus != null) { //If pressing shift
				//Vector3 towardsFocus = (focus - phone.transform.position).normalized;
				//Vector3 towardsPlayer = new Vector3 (transform.position.x - phone.transform.position.x, 0, transform.position.z - phone.transform.position.z).normalized;
				Quaternion rotation = Quaternion.LookRotation (focus - phone.transform.position);
				Vector3 localRotation = rotation.eulerAngles - transform.eulerAngles;
				if (localRotation.y <= 80) {
					//Debug.Log ("Dick");
					rotation = Quaternion.LookRotation (new Vector3 (rotation.eulerAngles.x, transform.eulerAngles.y + 80, rotation.eulerAngles.z));
				}
				phone.transform.rotation = Quaternion.Slerp (phone.transform.rotation, rotation, Time.deltaTime * 10);
				/*else if (phone.transform.localRotation.eulerAngles.y <= 80) {
					Debug.Log ("Ya");
					phone.transform.localRotation = Quaternion.Euler (phone.transform.localRotation.eulerAngles.x, 80, phone.transform.localRotation.eulerAngles.z);
				}*/
			}
		}

		//Controls the opening of the door by swiping
		if (openingDoor) {
			//rb.Sleep ();

			Vector3 currentMousePosition = Input.mousePosition;

			// Calculates how much the player swiped
			float deltaRotation = currentMousePosition.x - initMousePosition.x;

			// Calculates the target rotation as a result of the swipe
			Quaternion targetRotation = Quaternion.Euler (currentDoor.eulerAngles.x, currentDoorRotation - deltaRotation * doorOpenSensitivity, currentDoor.eulerAngles.z);

			float targetRotationY = targetRotation.eulerAngles.y;

			if (targetRotationY > 89 && targetRotationY < 181 && currentDoor.gameObject.GetComponent<Door>().rightOpened) //Prevents the door from making a full circular rotation
				// Rotates the door to the target rotation
				currentDoor.rotation = targetRotation;
			else if (targetRotationY < 271 && targetRotationY > 180 && !currentDoor.gameObject.GetComponent<Door>().rightOpened)
				currentDoor.rotation = targetRotation;
		}

		//Disables opening-door mode once the player releases the mouse left click
		if (openingDoor && Input.GetButtonUp ("Fire1")) {
			disableDoorOpen ();
		}

		//Pressing "escape" frees the mouse from the center of the screen
		if (Input.GetKeyDown("escape"))
			Cursor.lockState = CursorLockMode.None;
	}

	void FixedUpdate(){

		// Calculate the player's target velocity
		Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		targetVelocity = transform.TransformDirection(targetVelocity);
		targetVelocity *= speed;

		// Apply a force that attempts to reach the target velocity
		Vector3 velocity = rb.velocity;
		Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
		velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
		velocityChange.y = 0;
		rb.AddForce(velocityChange, ForceMode.VelocityChange);
	}

	//Detects player is in front of a door, and enables door-opening mode
	void OnTriggerStay(Collider other){
		if (other.gameObject.GetComponent<Door>() != null) {

			Vector3 rayOrigin = view.ViewportToWorldPoint (screenCenter);
			RaycastHit hit;
			bool hasTarget = Physics.Raycast (rayOrigin, view.transform.forward, out hit, selectDistance);

			if (!openingDoor && hasTarget && hit.transform.gameObject.GetComponent<Door>() != null && Input.GetButton ("Fire1")) {
				currentDoor = other.transform;
				openingDoor = true;
				camera.setState (false);
				Cursor.lockState = CursorLockMode.Confined;
				initMousePosition = Input.mousePosition;
				currentDoorRotation = currentDoor.eulerAngles.y;
			}
		}
	}

	//Detects player is no longer close to a door, and disables door-opening mode
	void OnTriggerExit(Collider other){
		if (other.gameObject.GetComponent<Door>() != null && openingDoor) {
			disableDoorOpen ();
		}
	}

	void disableDoorOpen(){
		openingDoor = false;
		currentDoor = null;
		Cursor.lockState = CursorLockMode.Locked;
		camera.setState (true);
	}
    void changeImage(Sprite change)//method to change the displayed Image in the UI
    {
        if (!inDocument)
        {
            Image temp = this.GetComponentInChildren<Image>();
            centerSprite = temp.sprite;
            temp.sprite = change;
            inDocument = true;
            colorOfCenter = temp.color;
            temp.color = Color.white;
            this.GetComponentInChildren<CanvasScaler>().scaleFactor = 100;
        }
    }
}

