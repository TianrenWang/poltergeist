﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour {

	public GameObject cube;
	public GameObject playerObject;
	public GameObject playerPhone;
	public GameObject dollObject;
	public static bool omnipotent;
	private static Camera dollCamera;
	private Camera playerCamera;
	public Item [] mirrors;
	public static GameObject trackerTemplate;
	public static GameObject player;
	public static Camera iPhone;
    public static GameObject doll;
	public static Dictionary<Item, bool> itemsVisibility;
	private static List<Item> dollWatch;
	private static Item[] items;

	// Use this for initialization
	void Awake () {
		dollWatch = new List<Item> ();
		omnipotent = true;
		doll = dollObject;
		dollCamera = null;
		playerCamera = playerObject.GetComponentInChildren<Camera> ();
		iPhone = playerPhone.GetComponentInChildren<Camera> ();
		items = GetComponentsInChildren<Item> ();
		itemsVisibility = new Dictionary<Item, bool> ();
		foreach (Item item in items) {
			itemsVisibility.Add (item, false);
		}
		trackerTemplate = cube;
		player = playerObject;
		setOmnipotence (omnipotent);
	}

	void Update(){
		setVisible (getItemsVisibleByPlayer (), itemsVisibility);
		if (!omnipotent)
			setPossessed (getItemsVisibleByDoll ());
		/*foreach (Item item in itemsVisibility.Keys) {
			Debug.Log (item.transform + "//" + itemsVisibility [item]);
		}*/
	}

	//Set the items in the argument as visible to the player
	private void setVisible(List<Item> items, Dictionary<Item, bool> itemsVisibility){
		List<Item> allItems = new List<Item>(itemsVisibility.Keys);
		foreach (Item item in allItems)
			itemsVisibility[item] = false;
		foreach (Item item in items)
			itemsVisibility [item] = true;
	}

	//Possess the items in the argument, and unpossess the unspecified items
	public static void setPossessed(List<Item> possessingItems){
		List<Item> allItems = new List<Item>(itemsVisibility.Keys);
		foreach (Item item in allItems)
			if (item.gameObject.GetComponent<BoxCollider>() != null && dollWatch.Contains(item) &&
                (possessingItems.Contains (item) || (item.getTracker() != null && possessingItems.Contains(item.getTracker())))) {
                if (item.mode != Item.Mode.Possessed)
                    item.setState (Item.Mode.Possessed);
			} else {
                if (item.mode == Item.Mode.Possessed)
                    item.setState (Item.Mode.Moveable);
			}
	}

	//Checks the map to see if the item in the argument is visible
	public bool isVisible(Item item){
		return itemsVisibility [item];
	}

	//Return a list of mirror items that are visible by the specified camera
	private List<Item> getVisibleMirrors(Camera camera){
		List<Item> visibleMirrors = new List<Item>();
		for (int i = 0; i < mirrors.Length; i++)
			if (mirrors [i].isVisibleBy (camera))
				visibleMirrors.Add(mirrors [i]);
		return visibleMirrors;
	}

	//Get all the items that are visible by the specified mirror. Do not use this for player.
	private List<Item> getVisibleItemsByCamera(Camera camera){
		//Creates a list of items that are visible by this camera
		List<Item> visibleItems = new List<Item> ();
		foreach (Item item in itemsVisibility.Keys) {
			if (item.isVisibleBy (camera))
				visibleItems.Add (item);
		}

		//For each item visible by the camera, create a dummy item in front of the mirror
		//The items returned here are dummy objects that has a reference to the original item
		List<Item> dummyItems = new List<Item>();
		foreach (Item item in visibleItems) {
			Transform itemTransform = item.transform;
			Collider mirrorCollider = camera.GetComponentInParent<BoxCollider> ();
			Vector3 rayDirection = camera.transform.position - itemTransform.position;
			RaycastHit hitInfo;
			Ray ray = new Ray (itemTransform.position, rayDirection);
			bool wasHit = mirrorCollider.Raycast (ray, out hitInfo, rayDirection.magnitude);
			//if (!wasHit)
				//Debug.Log ("nothing was hit with Raycast!");
			GameObject dummy = Instantiate (cube);
			dummy.transform.position = hitInfo.point;
			dummy.transform.localScale = item.transform.localScale;
			dummy.transform.rotation = item.transform.rotation;
			dummy.AddComponent<Item> ();
			dummy.AddComponent<Dummy> ();
			dummy.GetComponent<Dummy> ().parent = item;
			dummyItems.Add (dummy.GetComponent<Item> ());
		}
		return dummyItems;
	}

	//Get all items that are visible by an player
	private List<Item> getItemsVisibleByPlayer(){
		List<Item> finalVisibleItems = new List<Item> ();

		//Get all the items that are immediately visible to the player
		foreach (Item item in itemsVisibility.Keys) {
			if (item.isVisibleBy (playerCamera) || item.isVisibleBy(iPhone))
				finalVisibleItems.Add (item);
		}

		List<Item> visibleMirrors = getVisibleMirrors (playerCamera);

		//For each visible mirror, create the dummy items and check if they are visible by the main cameras
		foreach (Item mirror in visibleMirrors) {
			List<Item> visibleItemsByMirror = getVisibleItemsByCamera (mirror.gameObject.GetComponentInChildren<Camera> ());
			foreach (Item item in visibleItemsByMirror) {
				if (item.isVisibleBy (playerCamera) || item.isVisibleBy (iPhone)) {
					finalVisibleItems.Add (item.gameObject.GetComponent<Dummy> ().parent);
				}
				Destroy (item.gameObject);
			}
		}
		return finalVisibleItems;
	}

	private static List<Item> getItemsVisibleByDoll(){
		List<Item> finalVisibleItems = new List<Item> ();

		//Get all the items that are immediately visible to the doll
		foreach (Item item in itemsVisibility.Keys) {
			if (item.isVisibleBy (dollCamera) && item.isMoveable ())
				finalVisibleItems.Add (item);
			if (item.getTracker() != null && item.getTracker().isVisibleBy(dollCamera))
				finalVisibleItems.Add (item.getTracker());
		}

		return finalVisibleItems;
	}

	public static void setOmnipotence(bool state){
        if (state) {
			dollWatch = new List<Item> ();
			if (dollCamera != null)
            	Destroy (dollCamera.gameObject);
		} else {
            GameObject cameraObject = new GameObject("Doll Camera");
            dollCamera = cameraObject.AddComponent<Camera> ();
			dollCamera.depth -= 2;
			dollCamera.nearClipPlane = 0.01f;
            cameraObject.transform.parent = doll.transform;
            cameraObject.transform.localPosition = Vector3.zero;
			dollWatch = getItemsVisibleByDoll ();
		}
		omnipotent = state;
	}

	public static void clearPossession(){
		foreach (Item item in items) {
            if (item.mode == Item.Mode.Possessed)
                item.setState (Item.Mode.Moveable);
		}
	}
}
