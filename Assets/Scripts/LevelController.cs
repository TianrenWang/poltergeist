﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour {

	protected bool ending;
	protected Controller player;
	public Mission mission;

	// Use this for initialization
	void Start () {
		ending = false;
		player = GetComponentInChildren<Controller> ();
	}

	void Update(){
		if (mission.checkComplete()) {
			end ();
		}
	}

	protected virtual void end(){
		loadLevel ();
	}

	protected virtual void loadLevel(){
		SceneManager.LoadScene("_Day1", LoadSceneMode.Single);
	}
}
