﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : MonoBehaviour {
    private bool status = true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
	}
    //method for switching on/off the light
    public void switchLight()
    {
        if(status)
        {
            this.GetComponentInChildren<Light>().enabled = false;
            status = false;
        }else
        {
            this.GetComponentInChildren<Light>().enabled = true;
            status = true;
        }

    }
    public bool getStatus()
    {
        return status;
    }
}
