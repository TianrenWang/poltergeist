﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mission : MonoBehaviour {

	public Action [] actions;
	private LevelController level;
	public static Action currentAction;
	private int actionNumber;
	private bool completed;
    public static ActivateItem nextActivation;

	// Use this for initialization
	void Start () {
        nextActivation = null;
		actionNumber = -1;
		completed = false;
		level = GetComponentInParent<LevelController> ();
		next ();
	}

	public bool checkComplete(){
		return completed;
	}

	public void next(){
		//Destroy (currentAction.transform);
		actionNumber++;
		if (actionNumber == actions.Length)
			completed = true;
		else {
			currentAction = actions [actionNumber];
			currentAction.action ();
		}
		Debug.Log ("ActionNumber " + actionNumber + "/" + actions.Length);
	}
    public Action getCurrentAction() {
        return currentAction;
    }

	void Update(){
		if (completed)
			Debug.Log ("You passed the level");
	}
}
