﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionTexts : MonoBehaviour {

	public Transform player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (player);
		transform.eulerAngles = new Vector3 (0, transform.eulerAngles.y + 180, 0);
	}
}
