﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action : MonoBehaviour {

	public Objective [] objectives;
	public Item item;
    public Item.Mode mode;
    public List<Item> possessingItems;
	public Transform playerDestination;
	public Transform itemDestination;
	public bool omnipotent;
	public bool clearPossession;
	private GameObject player;
	private bool completed;
	private Dictionary<Objective, bool> OBJECTIVES;
	private Mission mission;
    private HauntedSpot[] hauntedSpots;

	// Use this for initialization
	void Start () {
		mission = GetComponentInParent<Mission> ();
		OBJECTIVES = new Dictionary<Objective, bool> ();
		foreach (Objective objective in objectives)
			OBJECTIVES.Add (objective, false);
		player = Utility.player;
		completed = false;
		if (!omnipotent) {
			hauntedSpots = GetComponentsInChildren<HauntedSpot> ();
		}
	}
	
	public void action(){

		if (clearPossession) {
			Utility.clearPossession ();
		}

		//Sets up the haunted spots and the omnipotent state of the Action
		if (hauntedSpots != null) {
			foreach (HauntedSpot spot in hauntedSpots) {
				spot.enabled = true;
			}
			if (omnipotent)
				throw new System.ArgumentException (transform + " - this Action has haunted spots but omnipotence is true. Either remove the haunted spots or make omnipotence = false");
			else
				Utility.setOmnipotence (false);
		} else {
			Utility.setOmnipotence (true);
		}

		//Modifies the specified item's property for this action
		if (item != null) {
            item.setState(mode);
			if (itemDestination != null){
				item.transform.position = itemDestination.transform.position;
				item.transform.rotation = itemDestination.transform.rotation;
			}
		}

		//Used for teleporting the player to a specified location for this action
		if (playerDestination != null) {
			player.transform.position = playerDestination.transform.position;
			player.transform.rotation = playerDestination.transform.rotation;
		}

        //Set the next activation-based objective
        ActivateItem activation = GetComponentInChildren<ActivateItem>();
        Mission.nextActivation = activation;

		//Move to the next action immediately if this action has no objectives
		if (objectives.Length == 0) {
			mission.next ();
			gameObject.SetActive (false);
		}
	}

	public void completeObjective(Objective objective){
		OBJECTIVES [objective] = true;
		if (checkCompleted ()) {
			if (hauntedSpots != null)
	            foreach (HauntedSpot spot in hauntedSpots)
	                spot.enabled = false;
			mission.next ();
			gameObject.SetActive(false);
		}
	}

	private bool checkCompleted(){
		foreach(KeyValuePair<Objective, bool> kvp in OBJECTIVES)
		{
			if (!kvp.Value)
				return false;
		}
		return true;
	}
	void Update(){
		//Debug.Log (completed);
	}
}
