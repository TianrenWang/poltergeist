﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class objectiveText : MonoBehaviour {
    private Text objectifText;
    public Mission mission;
    private Objective currentObjectif = null;
    private Action currentAction=null;
    private int currentIndex;
    
    // Use this for initialization
    void Start () {
        currentIndex = 0;
        objectifText = GetComponent<Text>();
        InvokeRepeating("Repeat", 2.0f, 5f);//method to repeat over time
    }
	
	// Update is called once per frame
	void Repeat () {
        if (currentAction != mission.getCurrentAction()) {// we check if we have changed to another action
            currentAction = mission.getCurrentAction();
            currentIndex = 0;
        }
        if (objectifText.enabled)
        {
            objectifText.enabled = false;
        }
        else
        {
            Objective[] currentobjectiflist =currentAction.objectives;
            for (int i=0; i < currentobjectiflist.Length; i++)
            {
                if(currentObjectif != currentobjectiflist[i] && currentIndex>= i)
                {
                    
                    currentObjectif = currentobjectiflist[i];
                    currentIndex++;
                    if (currentIndex == currentobjectiflist.Length)//if our currentIndex is outOfrange
                    {
                        currentIndex = 0;
                    }
                    break;


                }
                
                
            }
            objectifText.text = currentObjectif.objectiveMessage;
            objectifText.enabled = true;
        }
    }
}
