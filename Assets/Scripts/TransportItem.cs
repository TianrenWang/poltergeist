﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//In TransportItem objective, the player must move items in the room to their destination.
public class TransportItem : Objective {

	public TriggerBox[] triggers;
	private Dictionary <TriggerBox, bool> TRIGGERS;

	// Use this for initialization
	protected override void Start () {
        

        base.Start ();
        TRIGGERS = new Dictionary<TriggerBox, bool>();
		foreach (TriggerBox trigger in triggers)
			TRIGGERS.Add (trigger, false);
	}

	//Loops through the list of triggers associated with this objective
	//and checks one-by-one to see if all of them are finished
	private bool checkCompleted(){
		foreach(KeyValuePair<TriggerBox, bool> kvp in TRIGGERS)
		{
			if (!kvp.Value)
				return false;
		}
		return true;
	}

	public void setTrigger(TriggerBox trigger, bool complete){
		TRIGGERS [trigger] = complete;
		if (complete) {
			completed = checkCompleted();
			if (completed) {
				action.completeObjective (this);
			}
		}
	}
}
