﻿//This code controls the first-person style view of the player

using System.Collections;
using UnityEngine;

public class camMouseLook : MonoBehaviour {

	Vector2 mouseLook;
	Vector2 smoothV;
	public float sensitivity = 5.0f;
	public float smoothing = 2.0f;
	private bool normalState;
	private bool test;

	GameObject character;

	// Use this for initialization
	void Start () {
		normalState = true;
		character = this.transform.parent.gameObject;
		test = false;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.T))
			test = !test;
		if (normalState && !test)
		{
			var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
			md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
			smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
			smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
			mouseLook += smoothV;
			mouseLook.y = Mathf.Clamp(mouseLook.y,-90f,90f);

			transform.localRotation = Quaternion.AngleAxis (-mouseLook.y, Vector3.right);
			character.transform.localRotation = Quaternion.AngleAxis (mouseLook.x, Vector3.up);
		}
	}

	public void setState(bool state){
		normalState = state;
	}
}

