﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Each action has multiple objectives. An objective is one single goal that the player must achieve.
public class Objective : MonoBehaviour {

	protected bool completed;
	protected Action action;
    public string objectiveMessage;

	// Use this for initialization
	protected virtual void Start () {
        
		completed = false;
		action = GetComponentInParent<Action> ();
	}

	public bool getComplete(){
		return completed;
	}
}
