﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

	public bool rightOpened = true;

	// Use this for initialization
	void Start () {
		Renderer renderer = GetComponent<Renderer> ();
		Vector3 halfShift = transform.right * renderer.bounds.extents.x; //The vector3 that points towards the right of the transform by half the length of the door
		transform.localPosition = transform.localPosition - halfShift;
		transform.Translate (halfShift);
	}
}
