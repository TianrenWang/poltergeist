﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBox : MonoBehaviour {
	
	public Item[] items;
	private Dictionary <Item, bool> ITEMS;
	private TransportItem objective;
	private bool complete;
	private Action action;

	// Use this for initialization
	void Start () {
		action = GetComponentInParent<Action> ();
		ITEMS = new Dictionary<Item, bool> ();
		foreach (Item item in items)
			ITEMS.Add (item, false);
		objective = GetComponentInParent<TransportItem> ();
	}
	
	void OnTriggerEnter(Collider other){
		if (action != Mission.currentAction)
			return;
		Item item = other.GetComponent<Item> ();
		if (item != null && ITEMS.ContainsKey(item)) {
			ITEMS [item] = true;
		}
		if (!complete && checkCompleted ()) {
			objective.setTrigger (this, true);
			complete = true;
		}
	}

	void OnTriggerExit(Collider other){
		if (action != Mission.currentAction)
			return;
		Item item = other.GetComponent<Item> ();
		if (item != null && ITEMS.ContainsKey(item)) {
			ITEMS [item] = false;
		}
		if (complete) {
			complete = false;
			objective.setTrigger (this, false);
		}
	}

	private bool checkCompleted(){
		foreach( KeyValuePair<Item, bool> kvp in ITEMS )
		{
			if (!kvp.Value) {
				return false;
			}
		}
		return true;
	}
}
