﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirror : MonoBehaviour {

	private Transform mirrorCam;
	private Transform playerCam;
	private Camera camera;
	private Material material;
	private Plane mirror;
	private bool ignoreThis;

	void Start(){
		camera = GetComponentInChildren<Camera> ();
		camera.depth = Camera.main.depth - 1;
		int layer = gameObject.layer;
		camera.cullingMask = (layer - 1 << 0) | (15 << layer + 1);
		mirrorCam = camera.transform;
		playerCam = Camera.main.transform;
		material = GetComponent<Renderer>().material;

		if (camera.targetTexture != null)
		{
			camera.targetTexture.Release();
		}
		camera.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
		material.mainTexture = camera.targetTexture;
	}

	// Update is called once per frame
	void Update () {
		mirror = new Plane (Vector3.Cross (transform.right, transform.up), transform.position);
		cameraPosition ();
		cameraRotation ();
		//cameraProperty ();
		//setCameraMatrix ();
	}

	private void cameraPosition(){
		Vector3 projection = mirror.ClosestPointOnPlane(playerCam.position);
		Vector3 mirrorDirection = (projection - playerCam.position) * 2f;
		mirrorCam.position = playerCam.position + mirrorDirection;
	}

	private void cameraRotation(){

		//This piece of code does rotates the mirror camera independently from the player camera
		/*Vector3 dir = (transform.position - mirrorCam.position).normalized;
		dir = new Vector3 (dir.x, 0, dir.z);
		Quaternion rot = Quaternion.LookRotation (dir);
		rot.eulerAngles = new Vector3 (playerCam.transform.eulerAngles.x, rot.eulerAngles.y, 0);
		mirrorCam.transform.rotation = rot;*/

		//This piece of code reflects the mirro camera based on player camera
		Ray ray = new Ray(playerCam.position, playerCam.forward);
		float distance;
		mirror.Raycast (ray, out distance);
		Vector3 intersectionPoint = playerCam.position + playerCam.forward * distance;
		Vector3 dir = (intersectionPoint - mirrorCam.position).normalized;
		if (distance < 0) {
			dir = -dir;
			camera.nearClipPlane = 0.01f;
		} else {
			camera.nearClipPlane = (transform.position - mirrorCam.position).magnitude;
		}
		Quaternion rot = Quaternion.LookRotation (dir);
		mirrorCam.transform.rotation = rot;
	}

	private void cameraProperty(){
		camera.nearClipPlane = (transform.position - mirrorCam.position).magnitude + 0.5f;
	}

	private void setCameraMatrix(){
		bool d3d = SystemInfo.graphicsDeviceVersion.IndexOf("Direct3D") > -1;
		Matrix4x4 M = transform.localToWorldMatrix;
		Matrix4x4 V = camera.worldToCameraMatrix;
		Matrix4x4 P = camera.projectionMatrix;
		if (d3d) {
			// Invert Y for rendering to a render texture
			for (int i = 0; i < 4; i++) {
				P[1,i] = -P[1,i];
			}
			// Scale and bias from OpenGL -> D3D depth range
			for (int i = 0; i < 4; i++) {
				P[2,i] = P[2,i]*0.5f + P[3,i]*0.5f;
			}
		}
		Matrix4x4 MVP = P*V*M;
		material.SetMatrix ("_ActualMatrix", MVP);
	}
}