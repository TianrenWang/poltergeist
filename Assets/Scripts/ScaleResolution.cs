﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleResolution : MonoBehaviour {

	private Text text;
	private float textSizeFactor = 30/558;

	// Use this for initialization
	void Start () {
		float xRes = Screen.currentResolution.width;
		float yRes = Screen.currentResolution.height;
		text = GetComponentInChildren<Text> ();
		text.fontSize = (int)(yRes * textSizeFactor);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
