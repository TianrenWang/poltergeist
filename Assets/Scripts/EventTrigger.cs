﻿//This is the parent class of the scripts that control each levels' poltergeist's behavior
//The poltergeist is the one that is teleporting the items on the tables to random locations in the room

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventTrigger : MonoBehaviour {

	protected int counter;
	// Use this for initialization
	void Start () {
		counter = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (counter > 2) {
			troll ();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		Debug.Log (counter);
		if (other.transform.gameObject.tag.Equals("Item"))
		{
			counter++;
		}
	}

	void OnTriggerExit(Collider other)
	{
		Debug.Log ("Exit");
		if (other.transform.gameObject.tag.Equals("Item"))
		{
			counter--;
		}
	}

	void troll(){
		Collider[] eventObjects = Physics.OverlapBox(this.transform.position, this.GetComponent<BoxCollider>().size / 2.0f);
		if (counter == 4) {
			loadLevel();
		} else {
			respawn (eventObjects);
		}
	}

	protected virtual void loadLevel(){
		//Up to each script
	}

	protected virtual void respawn(Collider[] eventObjects){
		//Up to each script
	}
}
