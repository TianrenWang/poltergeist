﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HauntedSpot : MonoBehaviour {

	private Vector3 position;
	private Quaternion rotation;

	// Use this for initialization
	void Start () {
		//Set the position and rotation of the teleported doll
		position = transform.GetChild (0).position;
		rotation = transform.GetChild (0).rotation;

		//The object destroyed here is actually not necessary for gameplay, but it makes it easier for designer to visualize
		//the placement of the teleportation spot
		MeshRenderer[] renderers = transform.parent.GetComponentsInChildren<MeshRenderer> ();
		foreach (MeshRenderer renderer in renderers){
			Destroy (renderer.gameObject);
		}
        this.enabled = false;
	}

	void OnTriggerEnter(Collider other)
	{
        if (other.gameObject.tag.Equals("Player") && this.enabled){
            Item doll = Utility.doll.GetComponent<Item>();
			doll.modifyTracker(position, rotation);
			//Debug.Log (transform + " - " + position);
        }
	}
}
