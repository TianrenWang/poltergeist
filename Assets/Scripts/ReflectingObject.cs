﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectingObject : MonoBehaviour {

	private Renderer[] renderers;
	private Camera camera;

	protected virtual void Start(){
		renderers = GetComponentsInChildren<Renderer> ();
		camera = GetComponentInChildren<Camera> ();
	}

	// Update is called once per frame
	protected virtual void Update () {
		if (!isVisible ())
			camera.enabled = false;
		else
			camera.enabled = true;
	}

	protected bool isVisible(){
		foreach (Renderer renderer in renderers)
			if (renderer.isVisible)
				return true;
		return false;
	}
}
