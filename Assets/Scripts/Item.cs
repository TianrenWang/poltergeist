﻿using System.Collections;
using UnityEngine;

public class Item : MonoBehaviour
{
	public Item.Mode mode;

	private bool selected;
	protected Rigidbody rb;
	protected Renderer renderer;

	private Vector3 startingPosition;
	private Quaternion startingRotation;
	private Mesh mesh;
	private Vector3 visibleVertex;
    private Animator animator;

	//Tracker is an invisible cube that is used to prevent a level object from respawning
	//in the field of vision of the player
	private GameObject tracker;

	public enum Mode {Stationary, Possessed, Moveable, Activated, Document};

	// Use this for initialization
	protected virtual void Awake()
	{
        animator = GetComponent<Animator>();
		rb = this.GetComponent<Rigidbody> ();
		selected = false;
        renderer = GetComponentInChildren<MeshRenderer>();
		mesh = GetComponentInChildren<MeshFilter> ().mesh;
		if (mode == Mode.Possessed) {
			setState (Mode.Possessed);
		}
	}

	//Called by the Controller.cs to drop or grab an item
	public void setSelectState(bool state)
	{
		selected = state;
		if (state) {
			Destroy (rb);
			transform.localRotation = Quaternion.identity;
		}
		else{
			gameObject.AddComponent<Rigidbody> ();
			rb = GetComponent<Rigidbody> ();
		}
		//rb.velocity = Vector3.zero;
		//rb.angularVelocity = Vector3.zero;
	}

	public bool getSelectedState(){
		return selected;
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		//Debug.Log (transform + "///" + visibleVertex);

		Bounds itemBound = GetComponentInChildren<MeshRenderer> ().bounds;

        //Returns the item to its starting position if it is not visible
        //Debug.Log(gameObject + "--" + !Utility.itemsVisibility[this]);
		if (mode == Mode.Possessed && !selected && !itemBound.Contains (startingPosition) && rb.IsSleeping() 
			&& !Utility.itemsVisibility[this] && !Utility.itemsVisibility[tracker.GetComponent<Item>()]) {
			transform.position = startingPosition;
            transform.rotation = startingRotation;
		}
	}

	public Vector3 getStartingPosition (){
		return startingPosition;
	}

	public Quaternion getStartingRotation (){
		return startingRotation;
	}

	private bool isVisible(){
		return false;   
	}

	public bool isVisibleBy(Camera camera){
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
		Vector3 cameraPosition = camera.transform.position;
		Transform mirror = camera.transform.parent;

		//Necessary to check camera activity because its frustum will work regardless of its activity
		//Also checks if the item is within the camera frustum
        if (camera.gameObject.activeInHierarchy && GeometryUtility.TestPlanesAABB (planes, renderer.bounds)) {
			Plane mirrorPlane = new Plane (Vector3.Cross (mirror.right, mirror.up), mirror.position);
			int count = 0;
			if (!blocked (cameraPosition, visibleVertex, mirror, mirrorPlane)) {
				return true;
			}
			foreach (Vector3 vertex in mesh.vertices) {
				count++;
				/*RaycastHit hit;
				Vector3 actualVertexPosition = vertex + transform.position;
				Ray ray = new Ray (actualVertexPosition, cameraPosition - actualVertexPosition);
				bool intersect = Physics.Raycast (ray, out hit, (cameraPosition - actualVertexPosition).magnitude);
				if (intersect && hit.transform == camera.transform.parent) {
					return true;
				}*/

				Vector3 actualVertexPosition = vertex + transform.position;
				/*float distance;
				Ray ray = new Ray (actualVertexPosition, cameraPosition - actualVertexPosition);
				mirrorPlane.Raycast (ray, out distance);
				Vector3 origin = ray.origin + ray.direction * distance;*/
				if (!blocked(cameraPosition, actualVertexPosition, mirror, mirrorPlane)) {
					//Debug.Log (count);
					visibleVertex = actualVertexPosition;
					return true;
				}
			}
		}
		return false;
	}

	private bool blocked(Vector3 cameraPosition, Vector3 vertex, Transform mirror, Plane mirrorPlane){
		float distance;
		Ray ray = new Ray (vertex, cameraPosition - vertex);
		mirrorPlane.Raycast (ray, out distance);
		Vector3 origin = ray.origin + ray.direction * distance;
		return Physics.Linecast (origin, vertex);
	}

	public void setState(Item.Mode mode){
		if (mode == Mode.Possessed) {
			startingPosition = transform.position;
			startingRotation = transform.rotation;
			tracker = Instantiate (Utility.trackerTemplate);
			tracker.transform.position = startingPosition;
			tracker.transform.localScale = GetComponent<BoxCollider> ().bounds.size;
			tracker.transform.rotation = startingRotation;
			tracker.AddComponent<Item> ();
			Utility.itemsVisibility.Add (tracker.GetComponent<Item> (), false);
		} else {
			if (tracker != null && Utility.itemsVisibility.ContainsKey (tracker.GetComponent<Item> ())) {
				Utility.itemsVisibility.Remove (tracker.GetComponent<Item> ());
				Destroy (tracker);
			}
		}
		this.mode = mode;
	}

    public Item getTracker(){
        //Debug.Log(gameObject + "----" + (tracker != null));
        if (tracker != null)
            return tracker.GetComponent<Item>();
        return null;
    }

    public void modifyTracker(Vector3 position, Quaternion rotation){
        startingPosition = position;
        startingRotation = rotation;
        tracker.transform.position = position;
        tracker.transform.rotation = rotation;
    }

    public bool isMoveable(){
        return mode == Item.Mode.Possessed || mode == Item.Mode.Moveable;
    }

    public Animator getAnimator(){
        return animator;
    }
}


