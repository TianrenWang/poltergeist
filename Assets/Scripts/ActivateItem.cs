﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateItem : Objective {

    public Item item;

	// Use this for initialization
    protected override void Start()
    {
        base.Start();
    }
	
    public void activate(){
        action.completeObjective(this);
    }
}
